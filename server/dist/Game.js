class Game {
    constructor() {
        this.rooms = {};
    }
    addUser(room, user) {
        let userIndex = this.userIndex(room, user.userId);
        if (userIndex === -1) {
            this.rooms[room].users.push(user);
        }
        else {
            this.rooms[room].users[userIndex].sockets.push(user.sockets[0]);
            this.rooms[room].users[userIndex].username = user.username;
        }
    }
    removeUser(room, userId, socketId) {
        let userIndex = this.userIndex(room, userId);
        if (userIndex !== -1) {
            const sockets = this.rooms[room].users[userIndex].sockets;
            if (sockets.length > 1 && sockets.includes(socketId)) {
                this.rooms[room].users[userIndex].sockets = sockets.filter(item => item !== socketId);
            }
            else {
                this.rooms[room].users.splice(userIndex, 1);
            }
        }
        else {
            this.rooms[room].users.splice(userIndex, 1);
        }
    }
    getRoomUsers(room) {
        return this.rooms[room].users;
    }
    setUserReady(room, userId, ready) {
        const userIndex = this.userIndex(room, userId);
        if (userIndex > -1) {
            this.rooms[room].users[userIndex].ready = ready;
        }
    }
    checkAllUsersReady(room) {
        const users = this.getRoomUsers(room);
        const usersCount = users.length;
        const usersReady = users.filter((item) => {
            return item.ready !== false;
        });
        const usersReadyCount = usersReady.length;
        // return (usersCount === usersReadyCount) && (usersReadyCount > 1)
        return (usersCount === usersReadyCount);
    }
    findUser(room, userId) {
        const userIndex = this.userIndex(room, userId);
        if (userIndex > -1) {
            return this.rooms[room].users[userIndex];
        }
    }
    userIndex(room, userId) {
        return this.rooms[room].users.findIndex(item => item.userId === userId);
    }
    checkRoom(room) {
        return this.rooms.hasOwnProperty(room);
    }
    setRoomStatus(room, status) {
        if (this.rooms[room].status !== status) {
            this.rooms[room].status = status;
        }
    }
    getRoomStatus(room) {
        return this.rooms[room].status;
    }
    addRoom(room) {
        if (!this.checkRoom(room)) {
            console.log(`Создана комната ${room}`);
            this.rooms[room] = {
                users: [],
                status: 'lobby'
            };
        }
    }
}
export default Game;
