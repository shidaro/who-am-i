import express from 'express';
import { createServer } from 'http';
import { Server } from 'socket.io';
import registerHandlers from './handlers.js';
const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
    cors: {
        origin: "http://localhost:3000",
        credentials: true
    }
});
const onConnection = async (socket) => {
    registerHandlers(io, socket);
};
io.on('connection', onConnection);
httpServer.listen(3001);
