class User {
    constructor(id, name, socket) {
        this.id = id;
        this.name = name;
        this.sockets = [socket];
        this.ready = false;
    }
    pushSocket(socket) {
        this.sockets.push(socket);
    }
    setReady() {
        this.ready = true;
    }
    setUnReady() {
        this.ready = false;
    }
}
export default User;
