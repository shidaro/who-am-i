import Game from './Game.js';
import Room from './Room.js';
const game = new Game();
const registerHandlers = (io, socket) => {
    const query = socket.handshake.query;
    const count = io.engine.clientsCount;
    const room = query.room;
    const socketId = socket.id;
    const player = socket.data;
    console.log(`Вы присоеденились к серверу. Клиентов на сервере: ${count}`);
    console.log(`Handler SocketID: ${socketId}`);
    const playerConnect = async (payload) => {
        socket.join(room);
        payload.sockets = [socketId];
        payload.ready = false;
        game.addRoom(room);
        game.addUser(room, payload);
        getUser(payload);
        const newRoom = new Room(room);
        newRoom.addUser(payload.userId, payload.username, socketId);
        console.log(newRoom.users);
        const roomStatus = game.getRoomStatus(room);
        socket.emit('room:status', roomStatus);
        // getUsers(room)
    };
    const getUser = (user) => {
        const findUser = game.findUser(room, user.userId);
        if (typeof findUser !== 'undefined') {
            findUser.sockets.forEach((socketId) => {
                socket.to(socketId).emit('user:name', user.username);
            });
        }
    };
    const playerDisconnect = (userId) => {
        console.log(`Вы отключились от сервера.`);
        game.removeUser(room, userId, socketId);
        getUsers(room);
    };
    const userReady = (payload) => {
        game.setUserReady(room, payload.userId, payload.ready);
        getUsers(room);
        const check = game.checkAllUsersReady(room);
        if (check) {
            game.setRoomStatus(room, 'countdown');
            const roomStatus = game.getRoomStatus(room);
            io.in(room).emit('users:ready', roomStatus);
            let time = 10;
            let timerStart = setInterval(() => {
                io.in(room).emit('timer', time);
                time -= 1;
                if (time === 0) {
                    game.setRoomStatus(room, 'lobby');
                    const roomStatus = game.getRoomStatus(room);
                    io.in(room).emit('room:status', roomStatus);
                    clearInterval(timerStart);
                }
            }, 1000);
        }
    };
    const getUsers = (room) => {
        const usersArray = game.getRoomUsers(room);
        // usersArray.filter(user => delete user.userId)
        io.in(room).emit('users', usersArray);
        // socket.broadcast.emit('users', usersArray)
        // socket.emit('users', usersArray)
    };
    socket.on('user:disconnect', playerDisconnect);
    socket.on('user:connect', playerConnect);
    socket.on('user:ready', userReady);
    socket.on('user:get', getUser);
};
export default registerHandlers;
