import {Socket} from 'socket.io'
import Game from './Game.js'
import Room from "./Room.js"

const game = new Game()
const rooms = game.rooms

const userHandler = (io: any, socket: Socket) => {
    const query = socket.handshake.query
    const room: any = query.room
    const userId: any = query.id
    const username = query.username
    const socketId = socket.id

    const playerConnect = async () => {
        socket.join(room)
        rooms.addRoom(room)
        rooms.userEnter(room, {username, userId}, socketId)
        game.usersDistribution(room)

        sendRoom(io, room)

        console.log(`Пользователь ${username} подключился к комнате ${room}`)
    }

    const playerDisconnect = (id: string) => {
        rooms.userLeave(room, id, socketId)
        game.usersDistribution(room)

        sendGameRoom(io, room, userId)

        console.log(`Пользователь ${username} отключился от комнаты ${room}`)
    }

    const userReady = (user: any) => {
        rooms.setUserReady(room, user.userId, user.ready)
        sendRoom(io, room)

        game.checkRoomReady(room, () => {
            sendRoom(io, room)
            let time = 3
            const timer = setInterval(() => {
                io.in(room).emit('timer', time)
                time -= 1
                if (time <= 0) {
                    game.gameStart(room)
                    sendRoom(io, room)
                    clearInterval(timer)
                }
            }, 200)
        })
    }


    const wordGet = (word: string) => {
        game.usersWordDistribution(room, userId, word)
        sendGameRoom(io, room, userId)
    }

    socket.on('word:send', wordGet)
    socket.on('user:disconnect', playerDisconnect)
    socket.on('user:connect', playerConnect)
    socket.on('user:ready', userReady)
}

export default userHandler

const sendGameRoom = (socket: any, room: string, userId: string) => {
    let roomObject: Room | undefined = rooms.getRoom(room)
    if (roomObject) {
        Object.entries(roomObject.getUsers()).forEach((User) => {
            const user = User[1]

            let currentRoomObject = JSON.parse(JSON.stringify(roomObject))
            const forUserId = currentRoomObject.users[user.id].wordForUser.id
            const fromUserId = currentRoomObject.users[user.id].wordFromUser.id

            currentRoomObject.users[user.id].hiddenWord = ''

            if (currentRoomObject.users[forUserId]) {
                currentRoomObject.users[forUserId].word = ''
            }
            if (currentRoomObject.users[fromUserId]) {
                currentRoomObject.users[fromUserId].word = ''
            }

            socket.to(user.sockets).emit('room:get', currentRoomObject)
        })
    }
}

const sendRoom = (io: any, room: string) => {
    let roomObject: Room | undefined = rooms.getRoom(room)
    if (roomObject) {
        io.in(room).emit('room:get', roomObject)
    }
}