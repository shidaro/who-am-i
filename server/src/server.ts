import express from 'express'
import {createServer} from 'http'
import {Server, Socket} from 'socket.io'
import userHandler from './userHandler.js'

const app = express()
const httpServer = createServer(app)
const io = new Server(httpServer, {
    cors: {
        origin: "http://localhost:3000",
        credentials: true
    }
})

const onConnection = async (socket: Socket) => {
    userHandler(io, socket)
}

io.on('connection', onConnection)
httpServer.listen(3001)