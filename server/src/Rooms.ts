import Room from './Room.js'
import User from './User.js'

class Rooms {
    public rooms: {
        [key: string]: Room
    }

    constructor() {
        this.rooms = {}
    }

    userEnter(room: string, user: any, socket: string) {
        const findRoom = this.getRoom(room)
        if (findRoom) {
            findRoom.addUser(user.userId, user.username, socket)
        }
    }

    userLeave(room: string, id: string, socket: string) {
        const findRoom = this.getRoom(room)
        if (findRoom) {
            findRoom.removeUser(id, socket)
        }
    }

    setUserReady(room: string, id: string, ready: boolean) {
        const findRoom = this.getRoom(room)
        if (findRoom) {
            const findUser = findRoom.getUser(id)

            if (findUser) {
                if (ready !== null) {
                    findUser.ready = ready
                }
            }
        }
    }

    getRoom(name: string) {
        if (typeof this.rooms[name] !== 'undefined') {
            return this.rooms[name]
        }
    }

    addRoom(name: string): void {
        if (!this.getRoom(name)) {
            this.rooms[name] = new Room(name)
        }
    }

    checkRoomReady(room: string, callback: any) {
        const findRoom = this.getRoom(room)

        if (findRoom) {
            const users = findRoom.users
            const usersLength = Object.keys(users).length
            const usersReady = []

            for (const user in users) {
                const ready = users[user].ready
                if (ready) usersReady.push(ready)
            }

            if (usersLength === usersReady.length) {
                findRoom.setStatus('start')
                callback()
            }
        }
    }
}

export default Rooms

