import Room from "./Room";
import Rooms from "./Rooms";

class User {
    public id: string
    public username: string
    public sockets: string[]
    public ready: boolean
    public room: Room | null
    public word: string
    public hiddenWord: string | undefined
    public wordForUser: { id: string, username: string }
    public wordFromUser: { id: string, username: string }

    constructor(id: string, username: string, socket: string) {
        this.id = id
        this.username = username
        this.sockets = [socket]
        this.ready = false
        this.room = null
        this.word = ''
        this.hiddenWord = ''
        this.wordForUser = {
            id: '',
            username: ''
        }
        this.wordFromUser = {
            id: '',
            username: ''
        }
    }

    getName() {
        return this.username
    }

    setWord(word: string) {
        this.word = word
    }

    setHiddenWord(hiddenWord: string) {
        this.hiddenWord = hiddenWord
    }

    setWordForUser(user: User) {
        this.wordForUser.id = user.id
        this.wordForUser.username = user.username
    }

    setWordFromUser(user: User) {
        this.wordFromUser.id = user.id
        this.wordFromUser.username = user.username
    }

    connectRoom(roomId: string) {

    }

    pushSocket(socket: string): void {
        this.sockets.push(socket)
    }

    setReady(): void {
        this.ready = true
    }
}

export default User