import User from './User.js'

class Room {
    public name: string
    public status: string
    public users: {
        [key: string]: User
    }

    constructor(name: string) {
        this.name = name
        this.status = 'lobby'
        this.users = {}
    }

    createUser(id: string, name: string, socket: string): User {
        return new User(id, name, socket)
    }

    addUser(id: string, username: string, socket: string) {
        let findUser = this.getUser(id)

        if (findUser) {
            findUser.username = username
            findUser.sockets.push(socket)
        } else {
            this.users[id] = this.createUser(id, username, socket)
        }
    }

    removeUser(id: string, socket: string) {
        let findUser = this.getUser(id)

        if (findUser) {
            if (findUser.sockets.length > 1 && findUser.sockets.includes(socket)) {
                findUser.sockets = findUser.sockets.filter(item => item !== socket)
            } else {
                delete this.users[id]
            }
        } else {
            delete this.users[id]
        }
    }

    getUser(id: string): User | undefined {
        if (typeof this.users[id] !== 'undefined') {
            return this.users[id]
        }
    }

    getUsers() {
        return this.users
    }

    setStatus(status: string) {
        this.status = status
    }
}


export default Room