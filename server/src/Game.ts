import Rooms from './Rooms.js'
import User from "./User";

class Game {
    public rooms: Rooms

    constructor() {
        this.rooms = new Rooms()
    }

    gameStart(room: string) {
        const findRoom = this.rooms.getRoom(room)
        findRoom?.setStatus('start')
    }

    gameReady(room: string) {
        const findRoom = this.rooms.getRoom(room)
        findRoom?.setStatus('countdown')
    }

    checkRoomReady(room: string, callback: any) {
        const findRoom = this.rooms.getRoom(room)

        if (findRoom) {
            const users = findRoom.users
            const usersLength = Object.keys(users).length
            const usersReady = []

            for (const user in users) {
                const ready = users[user].ready
                if (ready) usersReady.push(ready)
            }

            if (usersLength === usersReady.length && usersReady.length > 1) {
                this.gameReady(room)
                callback()
            }
        }
    }

    usersDistribution(room: string) {
        const findRoom = this.rooms.getRoom(room)
        if (findRoom) {
            const usersArray = Object.keys(findRoom.getUsers())
            const usersLength = usersArray.length - 1

            for (let i = 0; i <= usersLength; i++) {
                const userId = usersArray[i]
                const user = findRoom.getUser(userId)

                let nextUser = usersArray[0]
                let prevUser = usersArray[usersLength]

                if (i < usersLength) {
                    nextUser = usersArray[i + 1]
                }
                if (i !== 0) {
                    prevUser = usersArray[i - 1]
                }

                const findNextUser = findRoom.getUser(nextUser)
                const findPrevUser = findRoom.getUser(prevUser)

                if (findNextUser && findPrevUser && user) {
                    user.setWordForUser(findNextUser)
                    user.setWordFromUser(findPrevUser)
                }
            }
        }
    }

    usersWordDistribution(room: string, userId: string, word: string) {
        const findRoom = this.rooms.getRoom(room)
        const findUser = findRoom?.getUser(userId)
        const forUserId = findUser?.wordForUser.id

        if (forUserId) {
            const findForUser = findRoom?.getUser(forUserId)
            findForUser?.setHiddenWord(word)
        }

        findUser?.setWord(word)
    }
}

export default Game