import {createSlice} from '@reduxjs/toolkit'

export const readySlice = createSlice({
    name: 'ready',
    initialState: {
        value: null
    },
    reducers: {
        setActive: function (state) {
            state.value = true
        },
        setUnActive: function (state) {
            state.value = false
        }
    }
})

export const {setActive, setUnActive} = readySlice.actions
export default readySlice.reducer