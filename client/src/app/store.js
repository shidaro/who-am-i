import {configureStore} from '@reduxjs/toolkit'
import readySlice from '../slices/readySlice'

export default configureStore({
    reducer: {
        ready: readySlice
    }
})