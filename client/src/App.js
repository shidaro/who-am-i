import './App.css'
import Home from './components/Home'
import Room from './components/Room'
import {Routes, Route} from 'react-router-dom'
import {AppStyled} from './styled/components';

function App() {
    return (
        <AppStyled className="App">
            <Routes>
                <Route path="/" element={<Home/>}></Route>
                <Route path="/:roomId" element={<Room/>}></Route>
            </Routes>
        </AppStyled>
    );
}

export default App
