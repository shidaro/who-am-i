import {useState} from 'react'
import {useLocalStorage} from '../hooks/useLocalStorage'
import {useNavigate} from 'react-router-dom'
import {HomeStyled} from '../styled/components'


function Home() {
    let [username, setUsername] = useLocalStorage('username', 'User')
    const navigate = useNavigate()
    const [roomId, setRoomId] = useState('free')
    username = username.toString().trim()

    const handleChangeName = (e) => {
        setUsername(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        navigate(roomId)
    }

    const handleChangeRoom = (e) => {
        setRoomId(e.target.value)
    }

    return (
        <HomeStyled className="Home">
            <div className="Home__text">Имя:</div>
            <input type="text" onChange={handleChangeName} value={username ?? 'User'}/>
            <div className="Home__text">Комната:</div>
            <form onSubmit={handleSubmit}>
                <select value={roomId} onChange={handleChangeRoom}>
                    <option value="free">Free</option>
                </select>
                <button>Войти</button>
            </form>
        </HomeStyled>
    )
}

export default Home
