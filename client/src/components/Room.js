import {useGame} from '../hooks/useGame'
import Players from './Players/Players'
import Timer from './Timer'
import Ready from './Ready'
import {useState} from 'react'
import {RoomStyled} from '../styled/components'

function Room() {
    const path = window.location.pathname.slice(1)
    const [readyClass, setReadyClass] = useState('not-active')
    const {username, roomId, ready, timer, roomStatus, room, user, userId, setReady, word, setWord} = useGame(path, setReadyClass)

    return (
        <RoomStyled className="Room">
            <div className="Room__text">Комната: {roomId}</div>
            <Players users={room.users} word={word} setWord={setWord} roomStatus={roomStatus} username={username} user={user}/>
            <Ready setReady={setReady} ready={ready} roomStatus={roomStatus}/>
            <Timer timer={timer} roomStatus={roomStatus}/>
        </RoomStyled>
    )
}

export default Room