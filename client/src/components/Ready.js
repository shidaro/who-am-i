import React from 'react'
import {ReadyStyled} from '../styled/components';

function Ready({setReady, ready, roomStatus}) {
    const onClickHandler = () => {
        if (ready) {
            setReady(false)
        } else {
            setReady(true)
        }
    }

    if (roomStatus === 'lobby') {
        return (
            <ReadyStyled className={`Ready ${ready ? 'active' : 'not-active'}`} onClick={() => onClickHandler()}>
                Готов
            </ReadyStyled>
        )
    }
}

export default Ready