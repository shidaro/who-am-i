import {useEffect, useState} from 'react'
import {TimerStyled} from '../styled/components';

function Timer({timer, roomStatus}) {
    if (roomStatus === 'countdown') {
        return (
            <TimerStyled className="Timer">
                <div className="Timer__time">{timer}</div>
            </TimerStyled>
        )
    }
}

export default Timer