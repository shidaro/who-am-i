import {PlayersStyled, PlayerStyled} from '../../styled/components'
import Word from './Word'

function Players({users, word, setWord, roomStatus, username, user}) {
    function Player() {
        let playersList = []

        for (const id in users) {
            let userObject = users[id]
            let readyClass = 'not-ready'
            user.ready ? readyClass = 'ready' : readyClass = 'not-ready'
            playersList.push(
                <PlayerStyled key={id} className={`Player ${readyClass}`}>
                    <div>{userObject.username}</div>
                    <div>{userObject.hiddenWord}</div>
                </PlayerStyled>
            )
        }
        return playersList
    }

    return (<>
        <PlayersStyled className="Players">
            <Player></Player>
        </PlayersStyled>
        <Word user={user} word={word} setWord={setWord} roomStatus={roomStatus}/>
    </>)
}

export default Players