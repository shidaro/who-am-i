import {WordStyled} from '../../styled/components'

function Word({word, setWord, roomStatus, user}) {
    if (roomStatus === 'lobby') {
        const handleChangeWord = (e) => {
            setWord(e.target.value)
        }

        return (
            <WordStyled className="Word">
                <div className="Word__text">Введите слово для игрока {user.wordForUser.username}:</div>
                <input type="text" autoFocus value={word} onChange={handleChangeWord}/>
            </WordStyled>
        )
    }
}

export default Word