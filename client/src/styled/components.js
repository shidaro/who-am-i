import styled from 'styled-components'

const mainColor = 'darkslateblue'
const secondColor = 'crimson'

export const AppStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 1rem;
  text-align: center;
`

export const RoomStyled = styled.div`{
  padding: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .Room__text {
    font-size: 1.5rem;
    font-weight: bold;
    text-align: center;
    padding-bottom: .5rem;
    color: ${mainColor};
  }
}`

export const HomeStyled = styled.div`
  background: ${mainColor};
  color: #fff;
  border-radius: 1rem;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  max-width: 400px;
  width: 100%;

  .Home__text {
    font-size: 1.5rem;
    font-weight: bold;
    text-align: center;
    padding-bottom: .5rem;
    margin-right: 1rem;
  }

  input[type="text"], select, button {
    font-size: 1.5rem;
    border-radius: 2rem;
    border: none;
    padding: .3rem 1rem;
    color: ${mainColor};
    font-weight: bold;
  }

  input[type="text"] {
    margin-bottom: 1rem;
  }

  form {
    display: flex;
    justify-content: space-between;
  }

  select {
    padding: .3rem 1rem;
    cursor: pointer;
    flex: 1;
    margin-right: 1rem;
  }

  button {
    font-weight: bold;
    padding: .3rem 1rem;
    cursor: pointer;
    margin-left: auto;
    transition: .25s;
    color: ${mainColor};

    &:hover {
      background: ${secondColor};
      color: #fff;
    }
  }
`

export const PlayersStyled = styled.ul`{
  list-style: none;
  padding-left: 0;
  margin-bottom: 0;
  display: flex;
  flex-wrap: wrap;
  column-gap: 1rem;
  row-gap: 1rem;
}`

export const PlayerStyled = styled.li`{
  background: brown;
  border-radius: 1rem;
  color: #fff;
  padding: 1rem;
  font-size: 1.5rem;
  font-weight: bold;
  width: fit-content;
  transition: background-color .25s;
  display: flex;
  flex-direction: column;

  &.ready {
    background: darkcyan;
  }
}`

export const ReadyStyled = styled.button`{
  border: none;
  border-radius: 1em;
  padding: 0.5em 1em;
  margin-top: 1rem;
  font-size: 1.5rem;
  transition: background-color .25s;
  cursor: pointer;
  outline: none;
  font-weight: bold;

  &:hover {
    background: aquamarine;
  }

  &.active {
    background: chartreuse;
  }
}`

export const TimerStyled = styled.div`{
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.22);
  display: flex;
  align-items: center;
  justify-content: center;

  &:before {
    content: '';
    background: #fff;
    width: 150px;
    height: 150px;
    left: 50%;
    top: 50%;
    border-radius: 150px;
  }

  .Timer__time {
    font-size: 80px;
    position: absolute;
    width: fit-content;
    font-weight: bold;
    color: #000;
    user-select: none;
  }
}`

export const WordStyled = styled.div`{
  margin-top: 1rem;
  background: crimson;
  padding: 1rem;
  border-radius: 1rem;

  .Word__text {
    font-size: 1.5rem;
    font-weight: bold;
    text-align: center;
    padding-bottom: .5rem;
    color: #fff;
  }

  input {
    font-size: 1.5rem;
    border-radius: 2rem;
    padding: .3rem 1rem;
    color: ${mainColor};
    font-weight: bold;
    border: solid 2px crimson;
    transition: .25s;

    &:focus {
      outline: none;
      border: solid 2px #000;
    }
  }
}`