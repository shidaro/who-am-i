import {useEffect} from "react"

export const useBeforeDisconnect = (value) => {
    const handle = (e) => {
        let returnValue

        if (typeof value === "function") {
            returnValue = value(e)
        } else {
            returnValue = value
        }

        if (returnValue) {
            e.preventDefault()
            e.returnValue = returnValue
        }

        return returnValue
    }

    useEffect(() => {
        window.addEventListener('beforeunload', handle)
        return () => window.removeEventListener('beforeunload', handle)
        // eslint-disable-next-linedwd
    }, [])
}