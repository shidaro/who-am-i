import {useEffect, useState, useRef} from "react"
import io from 'socket.io-client'
import {nanoid} from 'nanoid'
import {useLocalStorage} from './useLocalStorage'
import useDidMountEffect from './useDidMountEffect'

const SERVER_URL = 'ws://localhost:3001'

export const useGame = (roomId, setReadyClass) => {
    const [userId] = useLocalStorage('userId', nanoid(8))
    const socketRef = useRef(null)
    const [username, setUsername] = useLocalStorage('username', 'User')
    const [user, setUser] = useState({
        username: username,
        wordForUser: {
            username: ''
        }
    })
    const [ready, setReady] = useState(false)
    const [timer, setTimer] = useState(0)
    const [roomStatus, setRoomStatus] = useState('lobby')
    const [room, setRoom] = useState({})
    const [word, setWord] = useState('')

    useEffect(() => {
        if (socketRef.current == null) {
            socketRef.current = io(SERVER_URL, {
                transports: ['websocket'],
                upgrade: false,
                query: {
                    room: roomId,
                    username: username,
                    id: userId
                },
                cors: {
                    origin: "*:*",
                    credentials: true
                }
            })

            socketRef.current.on('room:get', (room) => {
                console.log(room)
                setReady(room.users[userId].ready)
                setRoomStatus(room.status)
                setRoom(room)

                setUser(room.users[userId])
                setUsername(room.users[userId].username)
                setWord(room.users[userId].word)
            })

            socketRef.current.emit('user:connect', {username, userId})

            socketRef.current.on('timer', (time) => {
                setTimer(time)
            })

            return () => {
                socketRef.current.on('disconnect', () => {
                    socketRef.current.disconnect()
                })
            }
        }
    }, [roomId, userId, timer, roomStatus, room])

    useDidMountEffect(() => {
        socketRef.current.emit('user:ready', {userId, ready})
    }, [ready])

    useDidMountEffect(() => {
        socketRef.current.emit('word:send', word)
    }, [word])

    window.addEventListener("beforeunload", (ev) => {
        socketRef.current.emit('user:disconnect', userId)
    })

    return {username, roomId, ready, setReady, timer, roomStatus, room, user, userId, word, setWord}
}